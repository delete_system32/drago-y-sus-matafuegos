# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = 'sutty-jekyll-theme'
  spec.version       = '0.2.5'
  spec.authors       = ['f']
  spec.email         = ['f@sutty.nl']

  spec.summary       = 'A theme with Sutty\'s design.'
  spec.homepage      = 'https://0xacab.org/sutty/jekyll/sutty-jekyll-theme'
  spec.license       = 'Nonstandard'

  spec.files         = Dir['assets/**/*',
                           '_layouts/**/*',
                           '_includes/**/*',
                           '_sass/**/*',
                           '_data/**/*',
                           'LICENSE*',
                           'README*']

  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]

  spec.metadata = {
    'bug_tracker_uri'   => "#{spec.homepage}/issues",
    'homepage_uri'      => spec.homepage,
    'source_code_uri'   => spec.homepage,
    'changelog_uri'     => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  spec.add_runtime_dependency 'jekyll', '~> 4.2.0'
  spec.add_runtime_dependency 'jekyll-relative-urls', '~> 0.0'
  spec.add_runtime_dependency 'jekyll-feed', '~> 0.9'
  spec.add_runtime_dependency 'jekyll-seo-tag', '~> 2.1'
  spec.add_runtime_dependency 'jekyll-images', '~> 0.2'
  spec.add_runtime_dependency 'jekyll-include-cache', '~> 0'
  spec.add_runtime_dependency 'sutty-liquid'

  spec.add_development_dependency 'bundler', '~> 2.1'
  spec.add_development_dependency 'rake', '~> 12.0'
end
