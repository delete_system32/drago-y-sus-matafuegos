# Changelog

## 0.1.2 - 2020-04-09

* You can put custom CSS in _sass/custom.css

## 0.1.1 - 2020-04-07

* Responsive images

## 0.1.0 - 2020-03-06

First release!
